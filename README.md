# SciTools Essential

## Objective

SciTools Essential is a set of free-to-use scripts (given as-is) to manage your SciTools Projects.

## create_database_advanced.bash

```
create_database_advanced.bash -db DATABASENAME.UND -cpp -gcc GCCPATH -clang CLANGPATH -platform PLATFORM -nosystemheaders -deletedb -debug
```

create_database_advanced creates an **empty SciTools Database for C++ Using GCC or CLANG**. You can elect to add the system headers in your project (or not) with the "-nosystemheaders" flag.

- **-db DATABASENAME.UND** : The file name of the SciTools Database to create
- **-cpp** : Setup C++ project
- **-gcc GCCPATH** : Optional, where is the gcc you used. Will extract and add system includes.
- **-clang CLANGPATH** : Optional, where is the clang you used. Will extract and add system includes.
- **-nocompiler** : Optional, will not add any compiler paths 
- **-platform PLATFORM** : Optional, set up a plaform, example "x86-64, arm, aarch64, aarch64_32". Default is x86-64.
- **-nosystemheaders** : Optional, will not add the C++ system headers to the und files.
- **-deletedb** : Optional, delete the .und directory first.
- **-debug** : Optional, will not delete the temporary files for debugging.

## zip_database.bash
```
zip_database.bash -db DATABASENAME.UND -zip OUTPUT.ZIP -debug -zipuncommittedonly -nozipsymlinks
```

zip_database zips all the content of the database and all the files it analyzed (included headers, system headers, etc...). zip_database will not zip the Analysis itself (the "big" binary). 

If you current folder is a git directory, you can elect not to archive the files saved in the git repository by using the flag "-zipuncommittedonly". 
- **Make sure you have added the system headers in your project in case you use this to do cross platform work.**.
- **Use only with database created with create_database_advanced.bash.**.
- **-db DATABASENAME.UND** : The file name of the SciTools Database (must exist)
- **-zip OUTPUT.ZIP** : The file where we will zip the projects and all the files it depends on.
- **-zipuncommittedonly** : We will only zip the uncommitted file(s) - in general generated files.
- **-nozipsymlinks** : turn OFF the addition of double files if a header is a symlink. This may result in analysis error on remote machines due to missing headers (the symlinks that were not packaged).
- **-debug** : Optional, will not delete the temporary files for debugging.

## extract_compilers_from_compilecommands.bash
```
extract_compilers_from_compilecommands.bash -json COMPILE_COMMANDS.JSON -debug
```

extract_compilers_from_compilecommands extracts the list of compilers used in a "complex" compile_commands.json

## filter_compilers_in_compilecommands.bash
```
filter_compilers_in_compilecommands.bash -json COMPILE_COMMANDS.JSON -out COMPILE_COMMANDS.JSON-compiler COMPILER1 -compiler COMPILER2 -debug
```

filter_compilers_in_compilecommands extracts commands that use the specified compilers and create a new compile_commands.json.

## create_htmlreport_from_sca.py
```
upython create_htmlreport_from_sca.py ...
```

upython create_htmlreport_from_sca creates an HTML Mock-Up report from the SciTools Understand's Codecheck output.

There are 2 methods possible:

- [ ] **_Prior to SciTools build 1188_**: Use the flags **results & ignoredresults & ignore**. This report will **NOT** be able to identify or show the links between ignores and ignore rules. This method is deprecated.

- [ ] **_After build 1188_**: Use the flag **resultsJSON** instead. This will allow you to map ignore rules and ignore.

The parameters are:

- **-db DATABASENAME.und** : SciTools Database) 
- **-configCSV CodeCheckExportConfig.csv** : SciTools Config File (CodeCheckExportConfig.csv or coverage.csv)) 
- **-configJSON SciToolsChecksConfiguration.json** : SciTools Config File (SciToolsChecksConfiguration.json)) 
- **-results CodeCheckResultByTable.csv** : SciTools Results File (CodeCheckResultByTable.csv)) 
- **-ignoredresults CodeCheckIgnoredResultByTable.csv** : SciTools Ignored Results File (CodeCheckIgnoredResultByTable.csv)) 
- **-ignore ignores.json** : SciTools Ignores File (ignores.json)) 
- **-resultsJSON results.json** : SciTools Results File (results.json)) 
]
- **-outputHTML OUTPUT.HTML** : Will create this file (Ex: OUTPUT.HTML) but also file.css (OUTPUT.HTML.css))

In general, the files passed as parameters are the results of the following commands:

- **CodeCheckExportConfig.csv** or **coverage.csv**: This is created by `und codecheck -coverage` (after 1188) or `und codecheck -exportConfig` (prior to 1188)
- **SciToolsChecksConfiguration.json**: This is the JSON configuration file under the `DATABASE/codecheck/configs` folder.
- **CodeCheckResultByTable.csv**:This is the output file from `und codecheck -ignoredonlyresults <CONFIGURATION> <OUT_DIR> -db <DB.UND>`
- **CodeCheckIgnoredResultByTable.csv**: This is the output file from `und codecheck -ignoredonlyresults <CONFIGURATION> <OUT_DIR> -db <DB.UND>`
- **ignores.json**: This is the ignore.json file under `DATABASE/codecheck` folder.
- **results.json**: The command is TBD with version 1188.

## remove_symlinks.bash
```
remove_symlinks.bash -dir DIR -pattern PATTERN
```

remove_symlinks removes all symlink in a folder. Why? If Symlinks exist in a repository, "und add -json" and "zip_database" will not work well together. Make sure to run remove_symlinks on your repo **BEFORE** running any `und add -json ...`. 

Explanation: The process handles the symlink files correctly and zip the "targets", but let's say you have an include (-I) pointing to /my_include. If include.h is found in /my_include, but include.h is really a symlink to /another_include/include.h, it will lead to an error. The process will "zip" /another_include/include.h, but when the analysis is re-run on a remote machine, it will not find include.h in /my_include, triggering a "missing include" error. So the analysis will fail unless you recreate those symlinks... We are currently working on a solution... 

- **-dir DIR** : Where to start (will run in subdirs)
- **-pattern PATTERN** : This is the pattern for "find -name PATTERN". We suggest "*.[hc]*" to find all .c and .h files.
