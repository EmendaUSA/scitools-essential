# ======================================================================================================
# --------------------------------------------------------------------------------
# FILE NAME       : create_htmlreport_from_sca.py
# VERSION         : 2.0 
# ORIGINAL AUTHOR : Stephane Raynaud (stephane@3dspider.com)
# -----------------------------------------------------------------------------------
# This file is provided as-is and with no support. 
# You have the right to use & to modify the file for your own and commercial use ...
# ... only if you do not remove the ORIGINAL AUTHOR Line with Stephane's information.
# -----------------------------------------------------------------------------------
# ======================================================================================================
import understand
import sys
import traceback
import re
import os
import subprocess
import argparse
import json
import understand
from pathlib import Path,PurePath
from datetime import datetime
# ======================================================================================================
# Function to split csv file that may look like this "data",data,"data data",data ... 
SplitPatterns = re.compile(r'([^,"]*(?:"([^"]|"")+"[^,"]*))+|([^,"]+"[^"]+")|([^,"]+)|("")')
def splitCSV(aString):
	result = []
	whatWeDealWith = re.findall(SplitPatterns, aString)
	for match in whatWeDealWith:
		for candidate in match:
			if candidate != '':
				resultstring = candidate.strip("\"")
				result.append(resultstring)
				break
	return result
# ======================================================================================================
# Function(s) to do some "path" stuff...
def get_absolute_path(path,root_path):
	absolute_dir = str(PurePath(Path(root_path).absolute().resolve()))
	if not (os.path.isabs(path)):
		full_path = os.path.join(absolute_dir,path)
		return str(PurePath(Path(full_path).absolute().resolve()))
	else:
		return str(PurePath(Path(path).absolute().resolve()))
def dir_exists(path):
	return os.path.exists(path) and os.path.isdir(path)
def file_exists(path):	
	return os.path.exists(path) and not os.path.isdir(path)
# ======================================================================================================
# Checkers:
# Checkers will be stored in a dictionary with index being the CheckID as a "" -> checkers[CheckID]
# Each element will have the following information:
# "CheckID"			: "" Check ID - Reference key that can be used
# "Description"		: "" check description
# "Category"		: "" but expected to be "Advisory"|"Required"|"Mandatory"
# "Supported"		: True|False 
# "Run"				: True|False 
# "Results"			:[]
# "IgnoredResults"	: []
# "IgnoredRules"	:[]
def load_checkers(fileCSV,fileJSON):
	result = {}
	checkerLines = []
	checkerLines = fileCSV.readlines()
	for checkerLine in checkerLines[1:]: # We ignore the first line
		checkerLine = checkerLine.replace("\n","").strip(" ")
		if checkerLine != "":
			elementsInLine = splitCSV(checkerLine)
			result[elementsInLine[0]] = {}
			result[elementsInLine[0]]["CheckID"] = elementsInLine[0]
			result[elementsInLine[0]]["Description"] = elementsInLine[1]
			result[elementsInLine[0]]["Category"] = elementsInLine[2]
			result[elementsInLine[0]]["Supported"] = elementsInLine[3].upper()=="YES"
			result[elementsInLine[0]]["Run"] = False
			result[elementsInLine[0]]["Results"] = []
			result[elementsInLine[0]]["IgnoredResults"] = []
			result[elementsInLine[0]]["IgnoredRules"] = []
	checkerJSON = json.load(fileJSON)
	configurationName = checkerJSON["name"]
	for checkID in checkerJSON["checks"]:
		result[checkID]["Run"] = True
	return result
# ======================================================================================================
# What are the ignore rules for the ignores rules.
IGNOREALL = 0
IGNOREFILE = 1
IGNOREDIR = 2
IGNORELINES = 3
# ======================================================================================================
# Ignored Rules:
# Ignored Rules will be stored in a list
# Each element will have the following information:
# "IgnoredRuleID"		: "" ID of the Ignored Rule
# "CheckID"				: "" ID of the Check it applies to
# "EntityUniqueName"	: "" Entity referred by this rule (uniquename)
# "Entity"				: entity object - Entity referred by this rule - May be None
# "Lines"				:[] list of Lines associated with this rule 
# "FileName"			: "" Directory or File Name associated with this rules (where it applies) - May be None
# "FileEntity"			: entity object - Entity referred by FileName - May be None if directory or no file specified
# "FileType"			: IGNOREALL | IGNOREFILE | IGNORELINES |IGNOREDIR scope of the rule.
# "Justification"		: "" Entered by user in GUI
# "overrideString"		: "" TBD, not sure what this is.
# "SpelledOut"			: "" Generated explanation of the rule.
def load_ignore_rules(db,file,checkers):
	result = []
	# We create a x-ref for file longname and files
	xRefFile = {}
	for fileX in db.ents("File"):
		xRefFile[fileX.longname()] = fileX
	# We need the path of the und, basically .. applied to und path.
	dbDir = get_absolute_path("..",args.db)
	# Now parsing the justification
	indexJustification = 1
	for ignore in json.load(file)["FalsePositives"]:
		aNewResult = { }
		aNewResult["IgnoredRuleID"] = "EXEMPT_"+str(indexJustification)
		indexJustification = indexJustification + 1
		aNewResult["CheckID"] = ignore["violationCheckID"]
		checkers[ignore["violationCheckID"]]["IgnoredRules"].append(aNewResult)
		aNewResult["EntityUniqueName"] = ignore["entityName"]
		aNewResult["Entity"] = db.lookup_uniquename(ignore["entityName"]) # Return None if not found.
		aNewResult["Lines"] = []
		Lines = ignore["violationLines"].split(",")
		if Lines:
			for Line in Lines:
				if Line != '':
					aNewResult["Lines"].append(int(Line))
		if ignore["fileName"] == "*":
			aNewResult["FileName"] = None
			aNewResult["FileEntity"] = None
			aNewResult["FileType"] = IGNOREALL
		else:
			aNewResult["FileName"] = get_absolute_path(ignore["fileName"],dbDir)
			if dir_exists(aNewResult["FileName"]):
				aNewResult["FileType"] = IGNOREDIR
				aNewResult["FileEntity"] = None
			else:
				if len(aNewResult["Lines"]) > 0:
					aNewResult["FileType"] = IGNORELINES
				else:
					aNewResult["FileType"] = IGNOREFILE
				aNewResult["FileEntity"] = xRefFile[aNewResult["FileName"]]
		aNewResult["Justification"] = ignore["noteString"]
		aNewResult["overrideString"] = ignore["overrideString"]
		# Explaining what will be ignored:
		aNewResult["SpelledOut"] = "We ignore '"+aNewResult["CheckID"]+"' "
		if aNewResult["Entity"] != None:
			aNewResult["SpelledOut"] = aNewResult["SpelledOut"] + "for all occurences of '"+aNewResult["Entity"].longname()+"' "
		if aNewResult["FileType"] == IGNOREDIR:
			aNewResult["SpelledOut"] = aNewResult["SpelledOut"] + "in all files under the following directory '"+aNewResult["FileName"]+"' "
		if aNewResult["FileType"] == IGNOREFILE:
			aNewResult["SpelledOut"] = aNewResult["SpelledOut"] + "in the following file '"+aNewResult["FileName"]+"' "
		if aNewResult["FileType"] == IGNORELINES:
			aNewResult["SpelledOut"] = aNewResult["SpelledOut"] + "in the following file '"+aNewResult["FileName"]+"' "
			aNewResult["SpelledOut"] = aNewResult["SpelledOut"] + "at Line(s) "
			for line in aNewResult["Lines"]:
				aNewResult["SpelledOut"] = aNewResult["SpelledOut"] + str(line) + " "
		result.append(aNewResult)
	return result
# ======================================================================================================
# Results:
# Results will be stored in a list
# Each element will have the following information:
# "FileName"				: "" File Name
# "FileEntity"				: entity object - Entity referred by FileName - May be None
# "Violation"				: "" Description of the violation including specific names.
# "Line"					: int - Line Number
# "Column"					: int - Column Number
# "EntityName"				: "" Entity Name - Not the UNIQUENAME
# "Kind"					: "" Kind of Entity
# "CheckID"					: "" Check ID
# "Check Name"				: "" Check Title
# "Check Short Description"	: "" Smaller Title
# "Severity"				: "" Customer Defined.
# "Ignored"					: True | False
# "IgnoredRuleID"			: "" Ignored Rule ID or None
def load_results(db,file,ignored,checkers,ignoredRules):
	# ----------------------------------------------
	xRefFile = {}
	for fileX in db.ents("File"):
		xRefFile[fileX.longname()] = fileX
	dbDir = get_absolute_path("..",args.db)
	# ----------------------------------------------
	result = []
	resultLines = file.readlines()
	for resultLine in resultLines[1:]:
		resultLine = resultLine.replace("\n","").strip(" ")
		if resultLine != '':
			elementsInLine = splitCSV(resultLine)
			aNewResult = { }
			aNewResult["FileName"] = get_absolute_path(elementsInLine[0],dbDir)
			aNewResult["FileEntity"] = xRefFile[aNewResult["FileName"]]
			aNewResult["Violation"] = elementsInLine[1]
			aNewResult["Line"] = int(elementsInLine[2])
			aNewResult["Column"] = int(elementsInLine[3])
			aNewResult["EntityName"] = elementsInLine[4]
			aNewResult["Kind"] = elementsInLine[5]
			aNewResult["CheckID"] = elementsInLine[6] 
			aNewResult["Check"] = checkers[aNewResult["CheckID"]]
			aNewResult["Check Name"] = elementsInLine[7]  
			aNewResult["Check Short Description"] = elementsInLine[8] 
			aNewResult["Severity"] = elementsInLine[9]
			aNewResult["Ignored"] = ignored
			aNewResult["IgnoredRuleID"] = "T.B.D."
			if len(elementsInLine)>10:
				aNewResult["Ignored"] = aNewResult["Ignored"] or (elementsInLine[10].upper() == "TRUE")
			if aNewResult["Ignored"]:
				checkers[aNewResult["CheckID"]]["IgnoredResults"].append(aNewResult)
			else:					
				checkers[aNewResult["CheckID"]]["Results"].append(aNewResult)
			results.append(aNewResult)
	return result
# ======================================================================================================
# Load Results, Ignored Results and Ignore Rules in one shot!
def load_json_results(db,resultsJSON,checkers,resultLines,ignoredRules):
	return
	# JSONData = json.load(resultsJSON)
	#We create a x-ref for file longname and files
	# xRefFile = {}
	# for fileX in db.ents("File"):
		# xRefFile[fileX.longname()] = fileX
	#We need the path of the und, basically .. applied to und path.
	# dbDir = get_absolute_path("..",args.db)
	#Now parsing the justification
	# indexJustification = 1
	# for ignore in JSONData["IgnoreRules"]:
		# aNewResult = { }
		# aNewResult["IgnoredRuleID"] = "EXEMPT_"+ignore["IgnoreID"]
		# indexJustification = indexJustification + 1
		# aNewResult["CheckID"] = ignore["violationCheckID"]
		# checkers[ignore["violationCheckID"]]["IgnoredRules"].append(aNewResult)
		# aNewResult["EntityUniqueName"] = ignore["EntityUniqueName"]
		# aNewResult["Entity"] = db.lookup_uniquename(ignore["EntityUniqueName"]) # Return None if not found.
		# aNewResult["Lines"] = []
		# Lines = ignore["ViolationLines"].split(",")
		# if Lines:
			# for Line in Lines:
				# if Line != '':
					# aNewResult["Lines"].append(int(Line))
		# if ignore["FileUniqueName"] == "*":
			# aNewResult["FileName"] = None
			# aNewResult["FileEntity"] = None
			# aNewResult["FileType"] = IGNOREALL
		# else:
			# aNewResult["FileName"] = get_absolute_path(ignore["fileName"],dbDir)
			# if dir_exists(aNewResult["FileName"]):
				# aNewResult["FileType"] = IGNOREDIR
				# aNewResult["FileEntity"] = None
			# else:
				# if len(aNewResult["Lines"]) > 0:
					# aNewResult["FileType"] = IGNORELINES
				# else:
					# aNewResult["FileType"] = IGNOREFILE
				# aNewResult["FileEntity"] = xRefFile[aNewResult["FileName"]]
		# aNewResult["Justification"] = ignore["noteString"]
		# aNewResult["overrideString"] = ignore["overrideString"]
		#Explaining what will be ignored:
		# aNewResult["SpelledOut"] = "We ignore '"+aNewResult["CheckID"]+"' "
		# if aNewResult["Entity"] != None:
			# aNewResult["SpelledOut"] = aNewResult["SpelledOut"] + "for all occurences of '"+aNewResult["Entity"].longname()+"' "
		# if aNewResult["FileType"] == IGNOREDIR:
			# aNewResult["SpelledOut"] = aNewResult["SpelledOut"] + "in all files under the following directory '"+aNewResult["FileName"]+"' "
		# if aNewResult["FileType"] == IGNOREFILE:
			# aNewResult["SpelledOut"] = aNewResult["SpelledOut"] + "in the following file '"+aNewResult["FileName"]+"' "
		# if aNewResult["FileType"] == IGNORELINES:
			# aNewResult["SpelledOut"] = aNewResult["SpelledOut"] + "in the following file '"+aNewResult["FileName"]+"' "
			# aNewResult["SpelledOut"] = aNewResult["SpelledOut"] + "at Line(s) "
			# for line in aNewResult["Lines"]:
				# aNewResult["SpelledOut"] = aNewResult["SpelledOut"] + str(line) + " "
		# result.append(aNewResult)

# ======================================================================================================
if __name__ == '__main__':
	try:
# ======================================================================================================
		parser = argparse.ArgumentParser()
		parser.add_argument("-db", help = "SciTools Database") 
		parser.add_argument("-configCSV", help = "SciTools Config File (CodeCheckExportConfig.csv)") 
		parser.add_argument("-configJSON", help = "SciTools Config File (SciToolsChecksConfiguration.json)") 
		group1 = parser.add_argument_group('group1', 'Prior to SciTools build 1188')
		group1.add_argument("-results", help = "SciTools Results File (CodeCheckResultByTable.csv)") 
		group1.add_argument("-ignoredresults", help = "SciTools Ignored Results File (CodeCheckIgnoredResultByTable.csv)") 
		group1.add_argument("-ignore", help = "SciTools Ignores File (ignores.json)") 
		group2 = parser.add_argument_group('group2', 'After SciTools build 1188 (included)')
		group2.add_argument("-resultsJSON", help = "SciTools Results File (complete JSON file, results.json)") 
		parser.add_argument("-outputHTML", help = "Will create this file (Ex: output.html) but also file.css (output.html.css)")
		args = parser.parse_args()
		if (args.resultsJSON) and (args.results or args.ignoredresults or args.ignore):
			exit(1)
	except BaseException as err:
		parser.print_help()
		exit(1)
	try:
# ======================================================================================================
# LOAD DATA 
# ======================================================================================================
		results = [] # This will contains the actual results of the analysis
		ignoredResults = [] # This will contains the ignored results of the analysis
# ======================================================================================================
		db = understand.open(args.db)
# ======================================================================================================
		# 1. Loading the Checkers.
		checkers = {} # This will contains the checkers
		with open(args.configCSV, 'r') as codeCheckExportConfig: 
			with open(args.configJSON, 'r') as SciToolsChecksConfiguration:
				checkers = load_checkers(codeCheckExportConfig,SciToolsChecksConfiguration)
# ======================================================================================================
		Pre1188 = False
		ignoredRules = []
		resultLines = []
		if args.results:
			Pre1188 = True
# ======================================================================================================
			# 2. Loading the Ignore Rules
			with open(args.ignore, 'r') as ignoresJSON:
				ignoredRules = load_ignore_rules(db,ignoresJSON,checkers)
			# 3. we parse the result(s): violations still active.
			with open(args.results, 'r') as CodeCheckResultByTable:
				resultLines = load_results(db,CodeCheckResultByTable,False,checkers,ignoredRules)
			# 4. we parse the violations that were ignored if present...
			if args.ignoredresults:
				with open(args.ignoredresults, 'r') as CodeCheckIgnoredResultByTable:
					resultLines = load_results(db,CodeCheckIgnoredResultByTable,True,checkers,ignoredRules)
# ======================================================================================================
		else:
			Pre1188 = False
			with open(args.resultsJSON, 'r') as resultsJSON:
				# 2. We load all in one shot with the JSON File.
				load_json_results(db,resultsJSON,checkers,resultLines,ignoredRules)
# ======================================================================================================
# MISRA PROCESSING : 
# ======================================================================================================
		# References: Some are obtained assuming we are running in the docker.
		configurationName = os.environ.get('CI_PROJECT_NAME') if os.environ.get('CI_PROJECT_NAME') else "[Unspecified]" # The configuration name that was run.
		configurationSHA = os.environ.get('CI_COMMIT_SHA') if os.environ.get('CI_COMMIT_SHA')  else "[Unspecified]" # The SHA/Revision
		SciToolsVersion = os.environ.get('CI_SCITOOLS_VERSION') if os.environ.get('CI_SCITOOLS_VERSION')  else "[Unspecified]" # The SciTools Version
		timeStamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S") + " (format is YYYY-MM-DD HH:MM:SS)" # When is the report created
		# Determine if Pass Or Fails for the 3 categories
		CATEGORY_MISRA_Result = {}
		GLOBAL_MISRA_Result = { "Passed":True, "nChecks":0, "nSupportedChecks":0, "nRunChecks":0, "nResults":0, "nIgnoredResults":0, "nRules":0 }
		categories = ["Mandatory","Required","Advisory"]
		# ================================================
		for category in categories:
			aResult = { "Passed":True, "nChecks":0, "nSupportedChecks":0, "nRunChecks":0, "nResults":0, "nIgnoredResults":0, "nRules":0 }
			CATEGORY_MISRA_Result[category] = aResult
		# ================================================
		for check in checkers.values():
			CATEGORY_MISRA_Result[check["Category"]]["nChecks"] = CATEGORY_MISRA_Result[check["Category"]]["nChecks"] + 1
			CATEGORY_MISRA_Result[check["Category"]]["nSupportedChecks"] = CATEGORY_MISRA_Result[check["Category"]]["nSupportedChecks"] + (1 if check["Supported"] else 0)
			CATEGORY_MISRA_Result[check["Category"]]["nRunChecks"] = CATEGORY_MISRA_Result[check["Category"]]["nRunChecks"] + (1 if check["Run"] else 0)
			CATEGORY_MISRA_Result[check["Category"]]["nResults"] = CATEGORY_MISRA_Result[check["Category"]]["nResults"] + len(check["Results"])
			CATEGORY_MISRA_Result[check["Category"]]["nIgnoredResults"] = CATEGORY_MISRA_Result[check["Category"]]["nIgnoredResults"] + len(check["IgnoredResults"])
			CATEGORY_MISRA_Result[check["Category"]]["nRules"] = CATEGORY_MISRA_Result[check["Category"]]["nRules"] + len(check["IgnoredRules"])
			GLOBAL_MISRA_Result["nChecks"] = GLOBAL_MISRA_Result["nChecks"] + 1
			GLOBAL_MISRA_Result["nSupportedChecks"] = GLOBAL_MISRA_Result["nSupportedChecks"] + (1 if check["Supported"] else 0)
			GLOBAL_MISRA_Result["nRunChecks"] = GLOBAL_MISRA_Result["nRunChecks"] + (1 if check["Run"] else 0)
			GLOBAL_MISRA_Result["nResults"] = GLOBAL_MISRA_Result["nResults"] + len(check["Results"])
			GLOBAL_MISRA_Result["nIgnoredResults"] = GLOBAL_MISRA_Result["nIgnoredResults"] + len(check["IgnoredResults"])
			GLOBAL_MISRA_Result["nRules"] = GLOBAL_MISRA_Result["nRules"] + len(check["IgnoredRules"])
		# ================================================
		for category in categories:
			if CATEGORY_MISRA_Result[category]["nResults"] > 0:
				CATEGORY_MISRA_Result[category]["Passed"] = False
		if (CATEGORY_MISRA_Result["Mandatory"]["nResults"]+CATEGORY_MISRA_Result["Required"]["nResults"]) > 0 :
			GLOBAL_MISRA_Result["Passed"] = False
# ======================================================================================================
# CSS CREATION:
# ======================================================================================================
		with open(args.outputHTML+".css", 'w') as HTMLPageCSS:
			HTMLPageCSS.write("html{ font:0.75em/1.5 sans-serif; color:#333; background-color:#fff; padding:1em; }\n")
			HTMLPageCSS.write("table{ width:100%; margin-bottom:1em; border-collapse: collapse;}\n")
			HTMLPageCSS.write("th{ font-weight:bold; border:1px solid #000; background-color:#ccc; }\n")
			HTMLPageCSS.write("td.Summary{ border:1px solid #000; text-align:center; }\n")
			HTMLPageCSS.write("td.SummaryFailed{ font-weight:bold; color:white; background-color:red; }\n")
			HTMLPageCSS.write("td.SummaryPassed{  color:white; background-color:green; }\n")
			HTMLPageCSS.write("td.SummaryBad{ border:2px solid red; color:red; }\n")
			HTMLPageCSS.write("td.SummaryAlert{ border:2px solid darkorange; color:darkorange; }\n")
			HTMLPageCSS.write("td.SummaryBold{ border:2px solid #000; font-weight:bold; }\n")
			HTMLPageCSS.write("td{ border:1px solid #ccc; }\n")
			HTMLPageCSS.write("th,td{ padding:0.5em; }\n")
			HTMLPageCSS.write("tr:nth-child(even) { background-color: #eee; }\n")
			HTMLPageCSS.write("caption { font-weight:bold; text-align:left; font-size:1.2em; padding-left:.5em; background-color: #000; color:white; padding:.2em }\n")
			HTMLPageCSS.write("th.CheckID { font-weight:bold; text-align:left; width:10em; }\n")
			HTMLPageCSS.write("td.CheckID { font-weight:bold; text-align:left; width:10em;}\n")
			HTMLPageCSS.write("th.IgnoredRuleID { color:darkgreen; font-weight:bold; text-align:left; width:10em; }\n")
			HTMLPageCSS.write("td.IgnoredRuleID { color:darkgreen; font-weight:bold; text-align:left; width:10em;}\n")
			HTMLPageCSS.write("th.Category { font-weight:bold; text-align:left; width:5em;}\n")
			HTMLPageCSS.write("td.Category { font-weight:bold; text-align:left; width:5em;}\n")
			HTMLPageCSS.write("th.Run { font-weight:bold; text-align:center; width:2em;}\n")
			HTMLPageCSS.write("td.Run { font-weight:bold; text-align:center; width:2em;}\n")
			HTMLPageCSS.write("th.Supported { font-weight:bold; text-align:center; width:2em;}\n")
			HTMLPageCSS.write("td.Supported { font-weight:bold; text-align:center; width:2em;}\n")
			HTMLPageCSS.write("th.File { text-align:right; width:40em;}\n")
			HTMLPageCSS.write("td.File { text-align:right; width:40em;}\n")
			HTMLPageCSS.write("th.Line { text-align:left; width:2em;}\n")
			HTMLPageCSS.write("td.Line { text-align:left; width:2em;}\n")
			HTMLPageCSS.write("th.Results { text-align:center; width:2em;}\n")
			HTMLPageCSS.write("td.Results { text-align:center; width:2em;}\n")
			HTMLPageCSS.write("td.ResultsX { font-weight:bold; font-size:1.2em; color:red; text-align:center; width:2em;}\n")
			HTMLPageCSS.write("th.IgnoredResults { text-align:center; width:2em;}\n")
			HTMLPageCSS.write("td.IgnoredResults { text-align:center; width:2em;}\n")
			HTMLPageCSS.write("td.IgnoredResultsX { font-weight:bold; font-size:1.2em; color:darkorange; text-align:center; width:2em;}\n")
			HTMLPageCSS.write("th.IgnoredRules { text-align:center; width:2em;}\n")
			HTMLPageCSS.write("td.IgnoredRules { text-align:center; width:2em;}\n")
# ======================================================================================================
# HTML CREATION:
# ======================================================================================================
		with open(args.outputHTML, 'w') as HTMLPage:
			HTMLPage.write("<HTML><head><link rel=\"stylesheet\" href=\""+args.outputHTML+".css\"></head><BODY>\n")
# ======================================================================================================
			# Run Information
			HTMLPage.write("<TABLE>\n")
			HTMLPage.write("<CAPTION>Execution and Tooling Information</CAPTION>\n")
			HTMLPage.write("<TBODY>\n")
			HTMLPage.write("<TR><TD>Project Name:</TD><TD>"+configurationName+"</TD>\n")
			HTMLPage.write("<TR><TD>Project Version:</TD><TD>"+configurationSHA+"</TD>\n")
			HTMLPage.write("<TR><TD>Time Stamp:</TD><TD>"+timeStamp+"</TD>\n")
			HTMLPage.write("<TR><TD>Tooling:</TD><TD>SciTools Understand "+SciToolsVersion+"</TD>\n")
			HTMLPage.write("</TBODY>\n")
			HTMLPage.write("</TABLE>\n")

			# Overview
			HTMLPage.write("<TABLE>\n")
			HTMLPage.write("<CAPTION>MISRA REPORT</CAPTION>\n")
			HTMLPage.write("<TBODY>\n")
			
			HTMLPage.write("<TR><TH></TH><TH>Result</TH><TH colspan=6>Details</TH></TR>")
			
			HTMLPage.write("<TR>")
			HTMLPage.write("<TH rowspan=2>Overall</TH>")
			HTMLPage.write("<TD class=\"Summary "+("SummaryPassed" if GLOBAL_MISRA_Result["Passed"] else "SummaryFailed")+"\" rowspan=2>"+("PASSED" if GLOBAL_MISRA_Result["Passed"] else "FAILED")+"</TD>")
			HTMLPage.write("<TH>Standard Checks</TH>")
			HTMLPage.write("<TH>Supported Checks</TH>")
			HTMLPage.write("<TH>Run Checks</TH>")
			HTMLPage.write("<TH>Defects</TH>")
			HTMLPage.write("<TH>Granted Deviations</TH>")
			HTMLPage.write("<TH>Justification Rules</TH>")
			HTMLPage.write("</TR>\n")
			HTMLPage.write("<TD class=\"Summary\">"+str(GLOBAL_MISRA_Result["nChecks"])+"</TD>")
			HTMLPage.write("<TD class=\"Summary\">"+str(GLOBAL_MISRA_Result["nSupportedChecks"])+" ("+format(GLOBAL_MISRA_Result["nSupportedChecks"]/GLOBAL_MISRA_Result["nChecks"]*100,'.2f')+"%)</TD>")
			HTMLPage.write("<TD class=\"Summary\">"+str(GLOBAL_MISRA_Result["nRunChecks"])+" ("+format(GLOBAL_MISRA_Result["nRunChecks"]/GLOBAL_MISRA_Result["nChecks"]*100,'.2f')+"%)</TD>")
			HTMLPage.write("<TD class=\"Summary"+(" SummaryBad" if GLOBAL_MISRA_Result["nResults"] > 0 else "")+"\">"+str(GLOBAL_MISRA_Result["nResults"])+"</TD>")
			HTMLPage.write("<TD class=\"Summary"+(" SummaryAlert" if GLOBAL_MISRA_Result["nIgnoredResults"] > 0 else "")+"\">"+str(GLOBAL_MISRA_Result["nIgnoredResults"])+"</TD>")
			HTMLPage.write("<TD class=\"Summary"+(" SummaryBold" if GLOBAL_MISRA_Result["nRules"] > 0 else "")+"\">"+str(GLOBAL_MISRA_Result["nRules"])+"</TD>")
			HTMLPage.write("</TR>\n")
			
			for category in categories:
				HTMLPage.write("<TR>")
				HTMLPage.write("<TH rowspan=2>"+category+"</TH>")
				HTMLPage.write("<TD class=\"Summary "+("SummaryPassed" if CATEGORY_MISRA_Result[category]["Passed"] else "SummaryFailed")+"\" rowspan=2>"+("PASSED" if CATEGORY_MISRA_Result[category]["Passed"] else "FAILED")+"</TD>")
				HTMLPage.write("<TH>Standard Checks</TH>")
				HTMLPage.write("<TH>Supported Checks</TH>")
				HTMLPage.write("<TH>Run Checks</TH>")
				HTMLPage.write("<TH>Defects</TH>")
				HTMLPage.write("<TH>Granted Deviations</TH>")
				HTMLPage.write("<TH>Justification Rules</TH>")
				HTMLPage.write("</TR>\n")
				HTMLPage.write("<TD class=\"Summary\">"+str(CATEGORY_MISRA_Result[category]["nChecks"])+"</TD>")
				HTMLPage.write("<TD class=\"Summary\">"+str(CATEGORY_MISRA_Result[category]["nSupportedChecks"])+" ("+format(CATEGORY_MISRA_Result[category]["nSupportedChecks"]/CATEGORY_MISRA_Result[category]["nChecks"]*100,'.2f')+"%)</TD>")
				HTMLPage.write("<TD class=\"Summary\">"+str(CATEGORY_MISRA_Result[category]["nRunChecks"])+" ("+format(CATEGORY_MISRA_Result[category]["nSupportedChecks"]/CATEGORY_MISRA_Result[category]["nChecks"]*100,'.2f')+"%)</TD>")
				HTMLPage.write("<TD class=\"Summary"+(" SummaryBad" if CATEGORY_MISRA_Result[category]["nResults"] > 0 else "")+"\">"+str(CATEGORY_MISRA_Result[category]["nResults"])+"</TD>")
				HTMLPage.write("<TD class=\"Summary"+(" SummaryAlert" if CATEGORY_MISRA_Result[category]["nIgnoredResults"] > 0 else "")+"\">"+str(CATEGORY_MISRA_Result[category]["nIgnoredResults"])+"</TD>")
				HTMLPage.write("<TD class=\"Summary"+(" SummaryBold" if CATEGORY_MISRA_Result[category]["nRules"] > 0 else "")+"\">"+str(CATEGORY_MISRA_Result[category]["nRules"])+"</TD>")
				HTMLPage.write("</TR>\n")
			
			HTMLPage.write("</TBODY>\n")
			HTMLPage.write("</TABLE>\n")

			# Checks with Error(s)
			HTMLPage.write("<TABLE>\n")
			HTMLPage.write("<CAPTION>Checks with Errors</CAPTION>\n")
			HTMLPage.write("<THEAD><TR>\n")
			HTMLPage.write("<TH class=\"CheckID\">CheckID</TH>")
			HTMLPage.write("<TH class=\"Description\">Check</TH>")
			HTMLPage.write("<TH class=\"Category\">Category</TH>")
			HTMLPage.write("<TH class=\"Supported\">Supported</TH>")
			HTMLPage.write("<TH class=\"Run\">Run</TH>")
			HTMLPage.write("<TH class=\"Results\">Errors</TH>")
			HTMLPage.write("<TH class=\"IgnoredResults\">Ignored</TH>")
			HTMLPage.write("<TH class=\"IgnoredRules\">Exception Rules</TH>\n")
			HTMLPage.write("</TR></THEAD>\n")
			HTMLPage.write("<TBODY>\n")
			sortedCheckers = sorted(checkers.values(), key=lambda x:x["CheckID"], reverse=False)
			sortedCheckers = list(filter(lambda x: ((len(x["Results"])+len(x["IgnoredResults"]))>0), sortedCheckers))  
			for checker in sortedCheckers:
				HTMLPage.write("<TR>")
				HTMLPage.write("<TD class=\"CheckID\">"+checker["CheckID"]+"</TD>")
				HTMLPage.write("<TD class=\"Description\">"+checker["Description"]+"</TD>")
				HTMLPage.write("<TD class=\"Category\">"+checker["Category"]+"</TD>")
				HTMLPage.write("<TD class=\"Supported\">"+("X" if checker["Supported"] else "")+"</TD>")
				HTMLPage.write("<TD class=\"Run\">"+("X" if checker["Run"] else "")+"</TD>")
				if len(checker["Results"]) > 0:
					HTMLPage.write("<TD class=\"ResultsX\">"+str(len(checker["Results"]))+"</TD>")
				else:
					HTMLPage.write("<TD class=\"Results\">"+str(len(checker["Results"]))+"</TD>")
				if len(checker["IgnoredResults"]) > 0:
					HTMLPage.write("<TD class=\"IgnoredResultsX\">"+str(len(checker["IgnoredResults"]))+"</TD>")
				else:
					HTMLPage.write("<TD class=\"IgnoredResults\">"+str(len(checker["IgnoredResults"]))+"</TD>")
				HTMLPage.write("<TD class=\"IgnoredRules\">"+str(len(checker["IgnoredRules"]))+"</TD>")
				HTMLPage.write("</TR>\n")
			HTMLPage.write("</TBODY>\n")
			HTMLPage.write("</TABLE>\n")
			
			# Print Errors
			HTMLPage.write("<TABLE>\n")
			HTMLPage.write("<CAPTION>Errors</CAPTION>\n")
			HTMLPage.write("<THEAD><TR>\n")
			HTMLPage.write("<TH class=\"CheckID\">CheckID</TH>")
			HTMLPage.write("<TH class=\"Category\">Category</TH>")
			HTMLPage.write("<TH class=\"File\">File</TH>")
			HTMLPage.write("<TH class=\"Line\">Line</TH>\n")
			HTMLPage.write("</TR></THEAD>\n")
			HTMLPage.write("<TBODY>\n")
			results = sorted(results, key=lambda x:(x["CheckID"],x["FileName"],int(x["Line"])), reverse=False)
			for result in results:
				if result["Ignored"] == False:
					HTMLPage.write("<TR>")
					HTMLPage.write("<TD class=\"CheckID\">"+result["CheckID"]+"</TD>")
					HTMLPage.write("<TD class=\"Category\">"+result["Check"]["Category"]+"</TD>")
					HTMLPage.write("<TD class=\"File\">"+result["FileName"]+"</TD>")
					HTMLPage.write("<TD class=\"Line\">"+str(result["Line"])+"</TD>")
					HTMLPage.write("</TR>\n")
			HTMLPage.write("</TBODY>\n")
			HTMLPage.write("</TABLE>\n")
			
			# Print Ignored Errors
			HTMLPage.write("<TABLE>\n")
			HTMLPage.write("<CAPTION>Granted Deviation</CAPTION>\n")
			HTMLPage.write("<THEAD><TR>\n")
			HTMLPage.write("<TH class=\"CheckID\">CheckID</TH>")
			HTMLPage.write("<TH class=\"Category\">Category</TH>")
			HTMLPage.write("<TH class=\"File\">File</TH>")
			HTMLPage.write("<TH class=\"Line\">Line</TH>")
			if not Pre1188:
				HTMLPage.write("<TH class=\"IgnoredRuleID\">Justification ID</TH>\n")
				HTMLPage.write("<TH class=\"Justification\">Justification</TH>\n")
			HTMLPage.write("</TR></THEAD>\n")
			HTMLPage.write("<TBODY>\n")
			results = sorted(results, key=lambda x:(x["CheckID"],x["FileName"],int(x["Line"])), reverse=False)
			for result in results:
				if result["Ignored"] == True:
					HTMLPage.write("<TR>")
					HTMLPage.write("<TD class=\"CheckID\">"+result["CheckID"]+"</TD>")
					HTMLPage.write("<TD class=\"Category\">"+result["Check"]["Category"]+"</TD>")
					HTMLPage.write("<TD class=\"File\">"+result["FileName"]+"</TD>")
					HTMLPage.write("<TD class=\"Line\">"+str(result["Line"])+"</TD>")
					if not Pre1188:
						HTMLPage.write("<TD class=\"IgnoredRuleID\">"+str(result["IgnoredRuleID"])+"</TD>")
						HTMLPage.write("<TD class=\"Justification\">"+""+"</TD>")
					HTMLPage.write("</TR>\n")
			HTMLPage.write("</TBODY>\n")
			HTMLPage.write("</TABLE>\n")
			
			# print Justification
			HTMLPage.write("<TABLE>\n")
			HTMLPage.write("<CAPTION>Justification</CAPTION>\n")
			HTMLPage.write("<THEAD><TR>\n")
			HTMLPage.write("<TH class=\"IgnoredRuleID\">Justification ID</TH>")
			HTMLPage.write("<TH class=\"CheckID\">CheckID</TH>")
			HTMLPage.write("<TH class=\"SpelledOut\">Ignore the following</TH>")
			HTMLPage.write("<TH class=\"Justification\">Justification</TH>\n")
			HTMLPage.write("</TR></THEAD>\n")
			HTMLPage.write("<TBODY>\n")
			ignoredRules = sorted(ignoredRules, key=lambda x:(x["CheckID"]), reverse=False)
			for ignore in ignoredRules:
				HTMLPage.write("<TR>")
				HTMLPage.write("<TD class=\"IgnoredRuleID\">"+ignore["IgnoredRuleID"]+"</TD>")
				HTMLPage.write("<TD class=\"CheckID\">"+ignore["CheckID"]+"</TD>")
				HTMLPage.write("<TD class=\"SpelledOut\">"+ignore["SpelledOut"]+"</TD>")
				HTMLPage.write("<TD class=\"Justification\">"+ignore["Justification"]+"</TD>")
				HTMLPage.write("</TR>\n")
			HTMLPage.write("</TBODY>\n")
			HTMLPage.write("</TABLE>\n")

			# Printing the checks
			HTMLPage.write("<TABLE>\n")
			HTMLPage.write("<CAPTION>Supported Checks</CAPTION>\n")
			HTMLPage.write("<THEAD><TR>\n")
			HTMLPage.write("<TH class=\"CheckID\">CheckID</TH>")
			HTMLPage.write("<TH class=\"Category\">Category</TH>")
			HTMLPage.write("<TH class=\"Run\">Run</TH>")
			HTMLPage.write("<TH class=\"Description\">Check</TH>")
			HTMLPage.write("<TH class=\"Results\">Errors</TH>")
			HTMLPage.write("<TH class=\"IgnoredResults\">Ignored</TH>")
			HTMLPage.write("<TH class=\"IgnoredRules\">Exception Rules</TH>\n")
			HTMLPage.write("</TR></THEAD>\n")
			HTMLPage.write("<TBODY>\n")
			sortedCheckers = sorted(checkers.values(), key=lambda x:x["CheckID"])
			sortedCheckers = list(filter(lambda x: x["Supported"], sortedCheckers))  
			for checker in sortedCheckers:
				HTMLPage.write("<TR>")
				HTMLPage.write("<TD class=\"CheckID\">"+checker["CheckID"]+"</TD>")
				HTMLPage.write("<TD class=\"Category\">"+checker["Category"]+"</TD>")
				HTMLPage.write("<TD class=\"Run\">"+("X" if checker["Run"] else "")+"</TD>")
				HTMLPage.write("<TD class=\"Description\">"+checker["Description"]+"</TD>")
				if len(checker["Results"]) > 0:
					HTMLPage.write("<TD class=\"ResultsX\">"+str(len(checker["Results"]))+"</TD>")
				else:
					HTMLPage.write("<TD class=\"Results\">"+str(len(checker["Results"]))+"</TD>")	
				if len(checker["IgnoredResults"]) > 0:
					HTMLPage.write("<TD class=\"IgnoredResultsX\">"+str(len(checker["IgnoredResults"]))+"</TD>")
				else:
					HTMLPage.write("<TD class=\"IgnoredResults\">"+str(len(checker["IgnoredResults"]))+"</TD>")
				HTMLPage.write("<TD class=\"IgnoredRules\">"+str(len(checker["IgnoredRules"]))+"</TD>")
				HTMLPage.write("</TR>\n")
			HTMLPage.write("</TBODY>\n")
			HTMLPage.write("</TABLE>\n")
			
			# Printing the checks
			HTMLPage.write("<TABLE>\n")
			HTMLPage.write("<CAPTION>Unsupported Checks</CAPTION>\n")
			HTMLPage.write("<THEAD><TR>\n")
			HTMLPage.write("<TH class=\"CheckID\">CheckID</TH>")
			HTMLPage.write("<TH class=\"Category\">Category</TH>")
			HTMLPage.write("<TH class=\"Description\">Check</TH>")
			HTMLPage.write("</TR></THEAD>\n")
			HTMLPage.write("<TBODY>\n")
			sortedCheckers = sorted(checkers.values(), key=lambda x:x["CheckID"])
			sortedCheckers = list(filter(lambda x: not x["Supported"], sortedCheckers))  
			for checker in sortedCheckers:
				HTMLPage.write("<TR>")
				HTMLPage.write("<TD class=\"CheckID\">"+checker["CheckID"]+"</TD>")
				HTMLPage.write("<TD class=\"Category\">"+checker["Category"]+"</TD>")
				HTMLPage.write("<TD class=\"Description\">"+checker["Description"]+"</TD>")
				HTMLPage.write("</TR>\n")
			HTMLPage.write("</TBODY>\n")
			HTMLPage.write("</TABLE>\n")
# ======================================================================================================
			HTMLPage.write("</BODY></HTML>\n")
# ======================================================================================================
	except understand.UnderstandError as err:
		print(traceback.format_exc())
		print(f"Understand Error {err=}")
	except BaseException as err:
		print(traceback.format_exc())
		print(f"Unexpected Error {err=}")
# ======================================================================================================

