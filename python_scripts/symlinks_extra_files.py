# ======================================================================================================
# This file can only be used/modified with the authorization of its author (Stephane Raynaud)
# Contact Stephane@3DSPIDER.com or Stephane.Raynaud@Emenda.com for more information.
# ======================================================================================================
# TO DO: 
# Add Windows version (maybe)? dir /AL /S C:\
import os
import sys
import argparse
import subprocess
import json
import shutil
from pathlib import Path,PurePath
import re
import traceback
import understand
from Scitools_Python_Essentials.dddhelpers import DDDHelper
# ======================================================================================================
# Extract List of Symlinks that needs to be added.
# Explanation
# In this explanation, link/include.h is a symlink to location/include.h
# Understand will store files at their locations, not the link. So it include.h is added
# to a project, it will be added as location/include.h. 
# Assuming your project is setup to use a -Ilink where it finds include.h, if the link is removed
# Then understand will not be able to find include.h
# This scripts extracts all potential symlinks in the -I of a UND project, and create
# the list of .h that may be included (basically a list of /link/include.h we think we might
# need. 
# It may add files we don't need, but this is as good as we can help.
# ======================================================================================================
def parseOutputOfUND(output):
	result = []
	resultDatas = output.split('\n')
	isI = False		# a "include"
	isS = False		# a "system" include
	isA = False		# a "direct" include
	file = ""			# File currently being 'specified'
	for data in resultDatas:
		trimedData = data.strip()
		trimedUpperData = trimedData.upper()
		if (trimedUpperData[:5] == 'FILE:'):
			file = trimedData[6:]
			if os.name == 'nt':
				file = file.replace("/","\\")
			continue
		if (trimedUpperData == 'C++ AUTO INCLUDES:'):
			isA = True
			continue
		if (trimedUpperData == 'INCLUDES:') or (trimedUpperData == "C++ INCLUDES:"):
			isI = True
			continue
		if (trimedUpperData == 'SYSTEM:'):
			isS = True
			continue
		if (trimedUpperData == ''):
			isI = False
			isS = False
			isA = False
			continue
		if (isI == True) and (trimedUpperData != 'NONE'):
			if not (trimedData in result):
				result.append(trimedData)
		if (isS == True) and (trimedUpperData != 'NONE'):
			if not (trimedData in result):
				result.append(trimedData)
	return result
# ======================================================================================================
if __name__ == '__main__':
	try:
# ======================================================================================================
		parser = argparse.ArgumentParser()
		parser.add_argument("-db", help = "SciTools Database", required=True) 
		parser.add_argument("-out", help = "Output file", required=True) 
		args = parser.parse_args()
	except BaseException as err:
		print("ERROR PARSING ARGUMENTS")
		print(err)
		parser.print_help()
		exit(-1)
# ======================================================================================================
	try:
		# -------------------------------------------------------
		listOfIncludes = []
		# -------------------------------------------------------
		helper = DDDHelper()
		# -------------------------------------------------------
		databaseName = os.path.abspath(args.db)
		db = understand.open(databaseName)
		if db:
			# -------------------------------------------------------
			# STEP1: We create the list of includes to check
			# -------------------------------------------------------
			listOfSourceFilesFile =  helper.get_absolute_path("files.txt",helper.scriptPath)
			with open(listOfSourceFilesFile, 'w') as listOf_f:
				sourceFiles = db.ents("C Code File")
				for sourceFile in sourceFiles:
					if os.name == 'nt':
						listOf_f.write(sourceFile.longname().replace("/","\\")+"\n")
					else:
						listOf_f.write(sourceFile.longname()+"\n")
			# -------------------------------------------------------
			# LIST OF INCLUDE (und list -all settings)
			undListAllCommands = "und list -all settings -db \""+databaseName+"\""
			results = helper.callProcess(undListAllCommands,2000,True)[0]
			if (results):
				listOfIncludes = parseOutputOfUND(results)
			# -------------------------------------------------------
			# LIST OF Specific INCLUDE (und list -override settings)
			undListOverrideCommands = "und list -override \"@"+listOfSourceFilesFile+"\" settings -db \""+databaseName+"\""
			#	For some reason, this command returns ERROR=1... probably because not all files have an override.
			# So we do not check it.
			proc = subprocess.run(undListOverrideCommands, text=True, shell=True, check=False, capture_output=True, timeout=2000)
			if (proc !=None) and (proc.stdout != None):
				listOfIncludes = listOfIncludes + parseOutputOfUND(proc.stdout)
			# -------------------------------------------------------
			# Remove duplicates
			listOfIncludes = list(dict.fromkeys(listOfIncludes))
			# -------------------------------------------------------
			# STEP2: We get the list of "SymLinks" that are in there.
			# -------------------------------------------------------
			# We create the list of all files in the db.
			# -------------------------------------------------------
			listOfAllFiles = {}
			files = db.ents("File")
			for sourceFile in files:
				listOfAllFiles[sourceFile.longname()]=True
			# -------------------------------------------------------
			# STEP3: We create the list of what need to be addded.
			# -------------------------------------------------------
			listOfFilesToAdd = {}
			countOfSymLinks = 0
			if os.name == 'nt':
				# Doesn't work on windows
				print("Windows Machine Detected. This script doesn't work here.")
			else:
				for directory in listOfIncludes:
					command = "find \""+directory+"\" -type l -printf '%p|%l\\n'"
					results = helper.callProcess(command,2000,True)[0]
					if results != None:
						for result in results.split('\n'):
							data = result.split('|') # data[0] = symlink, data[1] = link
							if data != None and len(data)>1:
								candidate = [data[0],helper.get_absolute_path(data[1],directory)]
								countOfSymLinks += 1
								if candidate[1] in listOfAllFiles.keys(): #it's in the project
									if not candidate[0] in listOfFilesToAdd.keys(): #We have not added it already.
										if not candidate[0] in listOfAllFiles.keys(): #it's not in the file that were already added by default
											listOfFilesToAdd[candidate[0]] = True 
			# -------------------------------------------------------
			# STEP4: We print the result (replacing current dir with .)
			# -------------------------------------------------------
			cwd = os.getcwd()
			with open(args.out, "w") as f:
				for file in listOfFilesToAdd.keys():
					fileLocal = file.replace(cwd,".")
					f.write(fileLocal+"\n")
			# -------------------------------------------------------
			print("Number of Includes             : " + str(len(listOfIncludes)))
			print("Possible symlinks identified   : " + str(countOfSymLinks))
			print("Possible links(file) added     : " + str(len(listOfFilesToAdd)))
			# -------------------------------------------------------
# ======================================================================================================
	except understand.UnderstandError as err:
		print(traceback.format_exc())
		print(f"Understand Error {err=}")
	except BaseException as err:
		print(traceback.format_exc())
		print(f"Unexpected Error {err=}")
# ======================================================================================================


		# #	LIST OF INCLUDE/MACROS (und list -all settings)
			# # generalSettings = None
			# # undListAllCommands = "\""+und+"\" list -all settings -db \""+databaseName+"\""
			# # if (logIt == True):
				# # print(self.Separator)
				# # print("Running the command to get general settings: " + undListAllCommands, flush=True)
			# # results = helper.callProcess(undListAllCommands,200,True)[0]
			# # if (results):
				# # self.parseOutputOfUND(results,compileCommands)
				
			# #LIST OF Specific INCLUDE/MACROS (und list -override settings)
			# # overrideSettings = None
			# # undListOverrideCommands = "\""+und+"\" list -override \"@"+listOfSourceFilesFile+"\" settings -db \""+databaseName+"\""
			# # if (logIt == True):
				# # print(self.Separator)
				# # print("Running the command to get override settings: " + undListOverrideCommands, flush=True)
			# # overrideResults = helper.callProcess(undListOverrideCommands,200,True)[0]
			# # if (overrideResults):
				# # self.parseOutputOfUND(overrideResults,compileCommands)
				
		# #	Now writing the compile_commands.json
			# # for compileCommand in compileCommands:
				# ........................................................
				# # arguments = []
				# ........................................................
				# # if compileCommands[compileCommand][4] == "C":
					# # if (".CPP" in compileCommand.upper()):
						# # if gpp != None and gpp != "":
							# # arguments.append(gpp)
						# # else:
							# # arguments.append("unspecified_C++_compiler")
					# # else:
						# # if gcc != None and gcc != "":
							# # arguments.append(gcc)
						# # else:
							# # arguments.append("unspecified_C_compiler")
					# # if compiler_options != None and compiler_options != "":
						# # compiler_options_array = compiler_options.split(" ")
						# # for x in compiler_options_array: # options
							# # arguments.append(x)
				# ........................................................
				# # if compileCommands[compileCommand][4] == "FORTRAN":
					# # if fortran != None and fortran != "":
						# # arguments.append(fortran)
					# # else:
						# # arguments.append("unspecified_fortran_compiler")
					# # if fortran_compiler_options != None and fortran_compiler_options != "":
						# # compiler_options_array = fortran_compiler_options.split(" ")
						# # for x in compiler_options_array: # options
							# # arguments.append(x)
				# ........................................................
				# # for x in compileCommands[compileCommand][0]: # I
					# # arguments.append("-I"+x)
				# # for x in compileCommands[compileCommand][1]: # D
					# # arguments.append("-D"+x)
				# # for x in compileCommands[compileCommand][2]: # S
					# # arguments.append("-I"+x)
				# # for x in compileCommands[compileCommand][3]: # A
					# # arguments.append("-include")
					# # arguments.append(x)
				# # arguments.append("-o")
				# # arguments.append(compileCommand+".o")
				# # arguments.append("-c")
				# # arguments.append(compileCommand)
				# # aFileData = {}
				# # aFileData["directory"] = databaseName
				# # aFileData["file"] = compileCommand
				# # aFileData["output"] = compileCommand+".o"
				# # aFileData["arguments"] = arguments
				# ........................................................
				# # TheNewJSON.append(aFileData)
				# ........................................................
			# # with open(compilecommandsFilename, 'w') as compilecommandsFile_f:
				# # compilecommandsFile_f.write(json.dumps(TheNewJSON, ensure_ascii=False, indent=2))
	# ......................................................................................................
	# # def __init__(self):
		# # self.version = "1.0"
	# # def getversion(self):
		# # return self.version
