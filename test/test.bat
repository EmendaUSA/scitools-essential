mkdir .\sca_results
und settings -C++AddFoundSystemFiles off -db .\scitools.und
und add -root DEPENDENCIES_USR=".\usr" -db .\scitools.und
und analyze .\scitools.und
und codecheck -coverage -showignored SciToolsChecksConfiguration  ./sca_results -db ".\scitools.und"
copy ".\scitools.und\codecheck\ignores.json" ".\sca_results"
copy ".\scitools.und\codecheck\configs\SciToolsChecksConfiguration.json" ".\sca_results"
upython "..\python_scripts\create_htmlreport_from_sca.py" -db ".\scitools.und" -configCSV ".\sca_results\coverage.csv" -configJSON ".\sca_results\SciToolsChecksConfiguration.json" -results ".\sca_results\CodeCheckResultByTable.csv" -ignore ".\sca_results\ignores.json" -outputHTML ".\output.html"
