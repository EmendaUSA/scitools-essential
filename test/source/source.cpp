// ======================================================================================================
// We include a system header (for scitools, this is a header included using #include <...>)
#include <iostream>
// We include a non-system header (for scitools, this is a header included using #include "...")
#include "source.h"
// We include a generated file (This file is generated at build time and define the PLAFORM macro.)
#include "config.h"

// ======================================================================================================
// In this section, we break a MISRA12 rule on purpose to illustrate ignores.

// BADMACRO: This violation is ignored for BADMACRO, so all violation (line 14 and 15) will be ignored.
#define BADMACRO
#undef BADMACRO // This is a violation of rules MISRA12 20.5 (File:source.cpp entity:AMACRO line: 14)
#undef BADMACRO // This is the same violation but on a different line (File:source.cpp entity:AMACRO line: 15)

// BADMACRO2: This violation is ignored only for Line 19 of file source.cpp, so line 20 will still trigger!
#define BADMACRO2
#undef BADMACRO2 // This is a violation of rules MISRA12 20.5 (File:source.cpp entity:AMACRO2 line: 19)
#undef BADMACRO2 // This is the same violation but on a different line (File:source.cpp entity:AMACRO line: 20)

// ======================================================================================================
int main() {
    // This 2 lines break MISRA 12 17.7
    // They are ignored for the whole file demo.cpp
    std::cout << "The Platform is PLATFORM";
    std::cout << "The Platform is PLATFORM";
    return 1;
}

// Finally, we also ignore the fact that BADMACRO is not used (MISRA 12 2.5) in the all directory
// This violation will still show for BADMACRO2
