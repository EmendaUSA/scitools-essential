#!/bin/bash
# --------------------------------------------------------------------------------
# FILE NAME       : filter_compilers_in_compilecommands.bash
# VERSION         : 2.0 
# ORIGINAL AUTHOR : Stephane Raynaud (stephane@3dspider.com)
# -----------------------------------------------------------------------------------
# This file is provided as-is and with no support. 
# You have the right to use & to modify the file for your own and commercial use ...
# ... only if you do not remove the ORIGINAL AUTHOR Line with Stephane's information.
# -----------------------------------------------------------------------------------
VERSION=v20
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DDD_INSTALL_PATH="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
DDD_CURRENT_DIR=`pwd`
echo "------------------------------------------------------------------"
echo "You are executing                 : $SOURCE"
echo "The location of the script is at  : $DDD_INSTALL_PATH"
echo "You are launching the script from : $DDD_CURRENT_DIR"
# --------------------------------------------------------------------------------
# Description / Usage
#
function usage() {
	echo "------------------------------------------------------------------"
	echo "ERROR: $1"
	echo "Usage: filter_compilers_in_compilecommands.bash -json COMPILE_COMMANDS.JSON -out COMPILE_COMMANDS.JSON-compiler COMPILER1 -compiler COMPILER2 -debug"
	echo "-json COMPILE_COMMANDS.JSON  : The file name of the compile_commands.json to parse"
	echo "-out COMPILE_COMMANDS.JSON   : The new compile_commands.json to create with only the compiler specified"
	echo "-compiler COMPILER           : full path of compiler to extract (to include in new compile_commands.json)"
    echo "-debug                       : Optional, will not delete the temporary files for debugging."
	echo "------------------------------------------------------------------"
	exit
	return
}
# --------------------------------------------------------------------------------
# Reading all the options from the command lines
#
# --------------------------------------------------------------------------------
COMPILERS=""
while [ -n "$1" ]; do # while loop starts
    case "$1" in
    -json) 
        JSON_FILE="$2"
        shift
        ;;
    -out) 
        JSON_OUT_FILE="$2"
        shift
        ;;
    -compiler) 
	if [ -z "$COMPILERS" ]
	then
		COMPILERS="$2"
	else
		COMPILERS="$2,$COMPILERS"
	fi
        shift
        ;;
    -debug)
        DEBUG="on"
        ;;
    *) usage "Option $1 not recognized" ; 
    esac
    shift
done
if [ -z "$JSON_FILE" ]
then
	usage "Please specify a json file to parse."
fi
if [ -z "$JSON_OUT_FILE" ]
then
	usage "Please specify a target json file."
fi

echo "$COMPILERS"

echo "------------------------------------------------------------------"
echo "I will create extract compilers from : $JSON_FILE"
echo "------------------------------------------------------------------"
echo "Creating a python script to 'filter' the compilers in /tmp/filter_compilers_in_compilecommands.py"
echo "import sys" >  /tmp/filter_compilers_in_compilecommands.py
echo "import traceback" >>  /tmp/filter_compilers_in_compilecommands.py
echo "import re" >>  /tmp/filter_compilers_in_compilecommands.py
echo "import json" >>  /tmp/filter_compilers_in_compilecommands.py
echo "if __name__ == '__main__':" >>  /tmp/filter_compilers_in_compilecommands.py
echo "  try:" >>  /tmp/filter_compilers_in_compilecommands.py
echo "    args = sys.argv" >>  /tmp/filter_compilers_in_compilecommands.py
echo "    new_data = []" >>  /tmp/filter_compilers_in_compilecommands.py
echo "    added = 0" >>  /tmp/filter_compilers_in_compilecommands.py
echo "    ignored = 0" >>  /tmp/filter_compilers_in_compilecommands.py
echo "    compilers = args[3].split(',')" >>  /tmp/filter_compilers_in_compilecommands.py
echo "    with open(args[1], 'r') as read_file:" >>  /tmp/filter_compilers_in_compilecommands.py
echo "      data = json.load(read_file)" >>  /tmp/filter_compilers_in_compilecommands.py
echo "      if data:" >>  /tmp/filter_compilers_in_compilecommands.py
echo "        for compile_command in data:" >>  /tmp/filter_compilers_in_compilecommands.py
echo "          pleaseAdd = False" >>  /tmp/filter_compilers_in_compilecommands.py
echo "          for compiler in compilers:" >>  /tmp/filter_compilers_in_compilecommands.py
echo "            if compiler in compile_command['arguments'][0]:" >>  /tmp/filter_compilers_in_compilecommands.py
echo "              pleaseAdd = True" >>  /tmp/filter_compilers_in_compilecommands.py
echo "          if pleaseAdd:" >>  /tmp/filter_compilers_in_compilecommands.py
echo "            added = added+1" >>  /tmp/filter_compilers_in_compilecommands.py
echo "            new_data.append(compile_command)" >>  /tmp/filter_compilers_in_compilecommands.py
echo "          else:" >>  /tmp/filter_compilers_in_compilecommands.py
echo "            ignored = ignored+1" >>  /tmp/filter_compilers_in_compilecommands.py
echo "    with open(args[2], 'w') as f:" >>  /tmp/filter_compilers_in_compilecommands.py
echo "      json.dump(new_data, f, indent=4, ensure_ascii=False)" >>  /tmp/filter_compilers_in_compilecommands.py
echo "    print('The new .json was created at '+args[2])" >>  /tmp/filter_compilers_in_compilecommands.py
echo "    print('We found '+str(added)+' compile commands that matched.')"  >>  /tmp/filter_compilers_in_compilecommands.py
echo "    print('We ignored '+str(ignored)+' compile commands that did not.')"  >>  /tmp/filter_compilers_in_compilecommands.py
echo "  except BaseException as err:" >>  /tmp/filter_compilers_in_compilecommands.py
echo "    print(traceback.format_exc())" >>  /tmp/filter_compilers_in_compilecommands.py
echo "    print(f'Unexpected Error {err=}')" >>  /tmp/filter_compilers_in_compilecommands.py
echo "------------------------------------------------------------------"
echo "Now executing the script at /tmp/filter_compilers_in_compilecommands.py"
upython /tmp/filter_compilers_in_compilecommands.py $JSON_FILE $JSON_OUT_FILE "$COMPILERS"
if [ -z "$DEBUG" ]
then
	echo "------------------------------------------------------------------"
	echo "cleaning up /tmp/filter_compilers_in_compilecommands.py"
        rm /tmp/filter_compilers_in_compilecommands.py 
fi

