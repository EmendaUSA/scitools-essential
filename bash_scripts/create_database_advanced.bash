#!/bin/bash
# -----------------------------------------------------------------------------------
# FILE NAME       : create_database_advanced.bash
# VERSION         : 2.0 
# ORIGINAL AUTHOR : Stephane Raynaud (stephane@3dspider.com)
#
# This will create a database project for later use in C/C++ Projects
# This must be used on Linux with bash
# This relies on the compiler being gcc or clang
# -----------------------------------------------------------------------------------
# This file is provided as-is and with no support. 
# You have the right to use & to modify the file for your own and commercial use ...
# ... only if you do not remove the ORIGINAL AUTHOR Line with Stephane's information.
# -----------------------------------------------------------------------------------
VERSION=v20
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DDD_INSTALL_PATH="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
DDD_CURRENT_DIR=`pwd`
echo "------------------------------------------------------------------"
echo "You are executing                 : $SOURCE"
echo "The location of the script is at  : $DDD_INSTALL_PATH"
echo "You are launching the script from : $DDD_CURRENT_DIR"
# --------------------------------------------------------------------------------
# Description / Usage
#
function usage() {
	echo "------------------------------------------------------------------"
	echo "ERROR: $1"
	echo "Usage: create_database_advanced.bash -db DATABASENAME.UND -cpp -java -gcc GCCPATH -clang CLANGPATH -platform PLATFORM -nosystemheaders -debug -ignore DIR"
	echo "-db DATABASENAME.UND   : The file name of the SciTools Database to create"
	echo "-cpp		     : Setup C++ project"
	echo "-gcc GCCPATH	     : Optional, where is the gcc you used. Will add gcc system path."
	echo "-clang CLANGPATH	 : Optional, where is the clang you used. Will add clang system path."
	echo "-platform	         : Optional, set platform, default is x86_64"
	echo "-nosystemheaders	 : Optional, will not add the C++ system headers to the und files."
	echo "-ignore            : Optional, possibly multiple, ignore a specific automatically added system directory."
	echo "-deletedb	         : Optional, delete the .und directory first."
	echo "-debug    	     : Optional, will not delete the temporary files for debugging."
	echo "WARNING: The script will DELETE the database if it exists."
	echo "------------------------------------------------------------------"
	exit
	return
}
# --------------------------------------------------------------------------------
# Reading all the options from the command lines
#
# --------------------------------------------------------------------------------
SYSTEM_HEADER_ONOFF=on
PLATFORM=x86_64-unknown-linux
# --------------------------------------------------------------------------------
while [ -n "$1" ]; do # while loop starts
    case "$1" in
    -db) 
        DDD_DATABASE="$2"
        shift
        ;;
    -gcc) 
        GCC_PATH="$2"
        shift
        ;;
    -clang) 
        CLANG_PATH="$2"
        shift
        ;;
    -platform) 
        PLATFORM="$2"
        shift
        ;;
    -nosystemheaders) 
        SYSTEM_HEADER_ONOFF="off"
        ;;
    -debug) 
        DEBUG="on" 
        ;;
    -ignore) 
		if [ -z "$IGNORES" ]
		then
			IGNORES="$2"
		else
			IGNORES="$2,$IGNORES"
		fi
			shift
        ;;
    -deletedb) 
        DELETEDB="on" 
        ;;
    -cpp) 
        CPP="c++" 
        ;;
    *) usage "Option $1 not recognized" ; 
    esac
    shift
done
if [ -z "$DDD_DATABASE" ]
then
	usage "Please specify a database"
fi
echo "------------------------------------------------------------------"
echo "I will create a database at : $DDD_DATABASE"
if [ -z "$DEBUG" ]
then
	echo "I will DELETE ALL the temporary files."
fi
if [ -n "$CPP" ]
then
	echo "I will add C++ Settings."
	if [ -n "$CLANG_PATH" ]
	then
		echo "I will extract system paths from gcc or clang at : $CLANG_PATH"
	fi	
	if [ -n "$GCC_PATH" ]
	then
		echo "I will extract system paths from gcc or clang at : $GCC_PATH"
	fi
	echo "I will add the system headers file to the database when analyzed : $SYSTEM_HEADER_ONOFF"
fi
# --------------------------------------------------------------------------------
# Setting Up...
#
if [ -n "$DELETEDB" ]
then
	echo "------------------------------------------------------------------"
	echo "I delete the database (if it exists) : $DDD_DATABASE"
	rm -Rf "$DDD_DATABASE"
fi
# ==================== [STEP 1] CREATE DB
echo "------------------------------------------------------------------"
echo "I create a bare minimum database at : $DDD_DATABASE"
und create -db "$DDD_DATABASE" -languages $CPP $JAVA 
# ==================== [STEP 2] ADD LOCAL DIRECTORY 
echo "------------------------------------------------------------------"
echo "I create a ./local directory for local analysis : $DDD_DATABASE/local"
mkdir -p "$DDD_DATABASE/local"
# ==================== [STEP 3.1] SETTING UP C++ OPTIONS
echo "------------------------------------------------------------------"
echo "I create a file with the desired options : $DDD_DATABASE/OPTION.txt"
touch "$DDD_DATABASE/OPTION.txt"
if [ -n "$CPP" ]
then 
	echo "Adding C++ Options..."
	if [ -n "$CLANG_PATH" ]
	then
		echo "-C++Compiler Clang" > "$DDD_DATABASE/OPTION.txt"
	else
		echo "-C++Compiler GCC" > "$DDD_DATABASE/OPTION.txt"
	fi
	echo "-C++UseStrict on" >> "$DDD_DATABASE/OPTION.txt"
	echo "-EnforcePortability on" >> "$DDD_DATABASE/OPTION.txt"
	echo "-C++Target $PLATFORM" >> "$DDD_DATABASE/OPTION.txt"
	echo "-C++CLanguageStandard gnu17" >> "$DDD_DATABASE/OPTION.txt"
	echo "-C++C++LanguageStandard gnu++17" >> "$DDD_DATABASE/OPTION.txt"
	echo "-C++CreateReferencesInInactiveCode off" >> "$DDD_DATABASE/OPTION.txt"
	echo "-C++SaveMacroExpansionText on" >> "$DDD_DATABASE/OPTION.txt"
	echo "-C++AddFoundFilesToProject on" >> "$DDD_DATABASE/OPTION.txt"
	echo "-C++AddFoundSystemFiles $SYSTEM_HEADER_ONOFF" >> "$DDD_DATABASE/OPTION.txt"
	echo "-C++SearchForIncludesInProject off" >> "$DDD_DATABASE/OPTION.txt"
	echo "-C++IgnoreDirectory off" >> "$DDD_DATABASE/OPTION.txt"
	echo "-C++CompareIncludesByContent off" >> "$DDD_DATABASE/OPTION.txt"
	echo "-C++AnalyzeHeadersInIsolation off" >> "$DDD_DATABASE/OPTION.txt"
fi
echo "------------------------------------------------------------------"
echo "This is the newly created options file: $DDD_DATABASE/OPTION.txt"
cat "$DDD_DATABASE/OPTION.txt"
# ==================== [STEP 3.3] INSERTING THE OPTIONS IN THE DATABASE USING 'und settings'
echo "------------------------------------------------------------------"
echo "Adding the options to the database using 'und settings'."
und settings @"$DDD_DATABASE/OPTION.txt" -db "$DDD_DATABASE"
# ==================== cleanup temporary files. 
if [ -z "$DEBUG" ]
then
	echo "------------------------------------------------------------------"
	echo "Cleaning OPTION.txt"
	rm "$DDD_DATABASE/OPTION.txt"
fi
# ==============================================================
if [ -n "$CPP" ]
then 
	if [ -n "$CLANG_PATH" ]
	then
		# ==================== [STEP 4.1] Extracting clang information to gcc.txt
		#  echo | clang -xc++ -E -v -
		echo "------------------------------------------------------------------"
		echo "Extracting clang information for $CLANG_PATH"
		echo | $CLANG_PATH -xc++ -E -v - > "$DDD_DATABASE/gcc.txt" 2>&1
		echo "------------------------------------------------------------------"
	fi
	if [ -n "$GCC_PATH" ]
	then
		# ==================== [STEP 4.1] Extracting gcc information to gcc.txt
		#  echo | gcc -xc -E -v -
		#  echo | gcc -xc++ -E -v -
		echo "------------------------------------------------------------------"
		echo "Extracting gcc information for $GCC_PATH"
		echo | $GCC_PATH -xc++ -E -v - > "$DDD_DATABASE/gcc.txt" 2>&1
		echo "------------------------------------------------------------------"
	fi
	if [ -n "$CLANG_PATH" ] || [ -n "$GCC_PATH" ]
	then
		echo "------------------------------------------------------------------"
		echo "We will not add the following system directory(ies) if found '$IGNORES'"
		echo "------------------------------------------------------------------"
		echo "The gcc includes will be (in gcc.txt.filtered):"
		# ==================== [STEP 4.2] creating script to convert gcc output to list of system directory
		echo "import io" > "$DDD_DATABASE/dirGcc.py"
		echo "import sys" >> "$DDD_DATABASE/dirGcc.py"
		echo "if __name__ == '__main__':" >> "$DDD_DATABASE/dirGcc.py"
		echo "    args = sys.argv" >> "$DDD_DATABASE/dirGcc.py"
		echo "    ignores=args[2].split(',')" >> "$DDD_DATABASE/dirGcc.py"
		echo "    with open(args[1]) as file:" >> "$DDD_DATABASE/dirGcc.py"
		echo "        lines = [line.strip() for line in file]" >> "$DDD_DATABASE/dirGcc.py"
		echo "    writeit = False" >> "$DDD_DATABASE/dirGcc.py"
		echo "    with open(args[1]+'.filtered', 'w') as the_file:" >> "$DDD_DATABASE/dirGcc.py"
		echo "        for line in lines:" >> "$DDD_DATABASE/dirGcc.py"
		echo "            if (line == '#include <...> search starts here:'):" >> "$DDD_DATABASE/dirGcc.py"
		echo "                writeit = True" >> "$DDD_DATABASE/dirGcc.py"
		echo "            else:" >> "$DDD_DATABASE/dirGcc.py"
		echo "                if (line == 'End of search list.'):" >> "$DDD_DATABASE/dirGcc.py"
		echo "                    writeit = False" >> "$DDD_DATABASE/dirGcc.py"
		echo "                else:" >> "$DDD_DATABASE/dirGcc.py"
		echo "                    if (writeit and (not(line in ignores))):" >> "$DDD_DATABASE/dirGcc.py"
		echo "                        the_file.write(line+'\n')" >> "$DDD_DATABASE/dirGcc.py"
		# ==================== [STEP 4.3] Using script to create a list of system directory from gcc.txt (created in 4.1)
		echo "Running the script and getting results in gcc.txt"
		upython "$DDD_DATABASE/dirGcc.py" "$DDD_DATABASE/gcc.txt" "$IGNORES"
		echo "------------------------------------------------------------------"
		echo "The gcc includes will be (in gcc.txt.filtered):"
		cat "$DDD_DATABASE/gcc.txt.filtered"
		# ==================== [STEP 4.4] Injecting the list of system directory into the database.
		echo "------------------------------------------------------------------"
		echo "Adding those directories to the database using 'und settings -c++_SystemIncludesAdd'"
		und settings -c++_SystemIncludesAdd @"$DDD_DATABASE/gcc.txt.filtered" -db "$DDD_DATABASE"
		# ==================== cleanup temporary files. 
		if [ -z "$DEBUG" ]
		then
			echo "------------------------------------------------------------------"
			echo "Cleaning dirGcc.py"
			rm "$DDD_DATABASE/dirGcc.py"
			echo "Cleaning gcc.txt"
			rm "$DDD_DATABASE/gcc.txt"
			echo "Cleaning gcc.txt.filtered"
			rm "$DDD_DATABASE/gcc.txt.filtered"
		fi
	fi
fi
# ==================== [STEP 6.1] Creating a script to enumerate /ROOT directories
echo "----------------------------------------------------------------"
echo "Creating a python script to list all the 'roots' directory like /home, /sys, /usr ...etc... "
echo "import os" > "$DDD_DATABASE/dirList.py"
echo "import re" >> "$DDD_DATABASE/dirList.py"
echo "if __name__ == '__main__':" >> "$DDD_DATABASE/dirList.py"
echo "  regex = re.compile('[^a-zA-Z0-9]')" >> "$DDD_DATABASE/dirList.py"
echo "  dirs = os.scandir('/')" >> "$DDD_DATABASE/dirList.py"
echo "  for dir in dirs:" >> "$DDD_DATABASE/dirList.py"
echo "      if dir.is_dir():" >> "$DDD_DATABASE/dirList.py"
echo "          dirpath = dir.path[1:len(dir.path)]" >> "$DDD_DATABASE/dirList.py"
echo "          dirpath_rootname ='DEPENDENCIES_'+(regex.sub('_',dirpath)).upper()" >> "$DDD_DATABASE/dirList.py"
echo "          print(dirpath_rootname+'=/'+dirpath)" >> "$DDD_DATABASE/dirList.py"
## ==================== [STEP 6.2] Executing script to create a list of Named Root and Dir Combination
echo "Running this script"
upython "$DDD_DATABASE/dirList.py" > "$DDD_DATABASE/dir.lst"
echo "----------------------------------------------------------------"
echo "We will add the following root dirs to the database:"
cat "$DDD_DATABASE/dir.lst"
## ==================== [STEP 6.3] Now defining all the nameroots in the database
echo "----------------------------------------------------------------"
echo "Adding the 'root' directories to the database."
## cat "$DDD_DATABASE/dir.lst" | while read in; do und add -root ${in} -db "$DDD_DATABASE"; done
und add -root @"$DDD_DATABASE/dir.lst" -db "$DDD_DATABASE"
## ==================== cleanup temporary files. 
if [ -z "$DEBUG" ]
then
       	echo "----------------------------------------------------------------"
       	echo "Cleaning dirList.py"
	rm "$DDD_DATABASE/dirList.py"
       	echo "Cleaning dir.txt"
	rm "$DDD_DATABASE/dir.lst"
fi

