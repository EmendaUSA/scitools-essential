#!/bin/bash
# --------------------------------------------------------------------------------
# FILE NAME       : extract_compilers_from_compilecommands.bash
# VERSION         : 2.0 
# ORIGINAL AUTHOR : Stephane Raynaud (stephane@3dspider.com)
# -----------------------------------------------------------------------------------
# This file is provided as-is and with no support. 
# You have the right to use & to modify the file for your own and commercial use ...
# ... only if you do not remove the ORIGINAL AUTHOR Line with Stephane's information.
# -----------------------------------------------------------------------------------
VERSION=v20
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DDD_INSTALL_PATH="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
DDD_CURRENT_DIR=`pwd`
echo "------------------------------------------------------------------"
echo "You are executing                 : $SOURCE"
echo "The location of the script is at  : $DDD_INSTALL_PATH"
echo "You are launching the script from : $DDD_CURRENT_DIR"
# --------------------------------------------------------------------------------
# Description / Usage
#
function usage() {
	echo "------------------------------------------------------------------"
	echo "ERROR: $1"
	echo "Usage: extract_compilers_from_compilecommands.bash -json COMPILE_COMMANDS.JSON -debug"
	echo "-json COMPILE_COMMANDS.JSON  : The file name of the compile_commands.json to parse"
    echo "-debug                       : Optional, will not delete the temporary files for debugging."
	echo "------------------------------------------------------------------"
	exit
	return
}
# --------------------------------------------------------------------------------
# Reading all the options from the command lines
#
# --------------------------------------------------------------------------------
while [ -n "$1" ]; do # while loop starts
    case "$1" in
    -json) 
        JSON_FILE="$2"
        shift
        ;;
    -debug)
        DEBUG="on"
        ;;
    *) usage "Option $1 not recognized" ; 
    esac
    shift
done
if [ -z "$JSON_FILE" ]
then
	usage "Please specify a database"
fi
echo "------------------------------------------------------------------"
echo "I will create extract compilers from : $JSON_FILE"
echo "------------------------------------------------------------------"
echo "Creating a python script to 'extract' information in /tmp/extract_compilers_from_compilecommands.py"
echo "import sys" > /tmp/extract_compilers_from_compilecommands.py
echo "import traceback" >> /tmp/extract_compilers_from_compilecommands.py
echo "import re" >> /tmp/extract_compilers_from_compilecommands.py
echo "import json" >> /tmp/extract_compilers_from_compilecommands.py
echo "if __name__ == '__main__':" >> /tmp/extract_compilers_from_compilecommands.py
echo "    try:" >> /tmp/extract_compilers_from_compilecommands.py
echo "        args = sys.argv" >> /tmp/extract_compilers_from_compilecommands.py
echo "        with open(args[1], 'r') as read_file:" >> /tmp/extract_compilers_from_compilecommands.py
echo "            data = json.load(read_file)" >> /tmp/extract_compilers_from_compilecommands.py
echo "        cache = {}" >> /tmp/extract_compilers_from_compilecommands.py
echo "        if data:" >> /tmp/extract_compilers_from_compilecommands.py
echo "            for compile_command in data:" >> /tmp/extract_compilers_from_compilecommands.py
echo "                compiler = compile_command['arguments'][0]" >> /tmp/extract_compilers_from_compilecommands.py
echo "                if not compiler in cache.keys():" >> /tmp/extract_compilers_from_compilecommands.py
echo "                    print(compiler)" >> /tmp/extract_compilers_from_compilecommands.py
echo "                    cache[compiler] = True" >> /tmp/extract_compilers_from_compilecommands.py
echo "    except BaseException as err:" >> /tmp/extract_compilers_from_compilecommands.py
echo "        print(traceback.format_exc())" >> /tmp/extract_compilers_from_compilecommands.py
echo "        print(f'Unexpected Error {err=}')" >> /tmp/extract_compilers_from_compilecommands.py
echo "------------------------------------------------------------------"
echo "Now executing the script at /tmp/extract_compilers_from_compilecommands.py"
echo "------------------------------------------------------------------"
echo "The compilers are:"
echo ""
upython /tmp/extract_compilers_from_compilecommands.py $JSON_FILE
echo ""
echo "------------------------------------------------------------------"
if [ -z "$DEBUG" ]
then
	echo "cleaning up /tmpextract_compilers_from_compilecommands.py"
        rm /tmp/extract_compilers_from_compilecommands.py 
fi

