#!/bin/bash
# --------------------------------------------------------------------------------
# FILE NAME       : remove_symlinks.bash
# VERSION         : 2.0 
# ORIGINAL AUTHOR : Stephane Raynaud (stephane@3dspider.com)
# -----------------------------------------------------------------------------------
# This file is provided as-is and with no support. 
# You have the right to use & to modify the file for your own and commercial use ...
# ... only if you do not remove the ORIGINAL AUTHOR Line with Stephane's information.
# -----------------------------------------------------------------------------------

VERSION=v20
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DDD_INSTALL_PATH="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
DDD_CURRENT_DIR=`pwd`
echo "------------------------------------------------------------------"
echo "You are executing                 : $SOURCE"
echo "The location of the script is at  : $DDD_INSTALL_PATH"
echo "You are launching the script from : $DDD_CURRENT_DIR"
# --------------------------------------------------------------------------------
# Description / Usage
function usage() {
	echo "------------------------------------------------------------------"
	echo "ERROR: $1"
	echo "Usage: remove_symlinks.bash -dir DIR -pattern PATTERN"
	echo "-dir DIR         : Where to start (will run in subdirs)"
	echo "-pattern PATTERN : This is the pattern for ""find -name PATTERN"" Suggestion ""*.[hc]*""" 
	echo "------------------------------------------------------------------"
	exit
	return
}
# --------------------------------------------------------------------------------
# Reading all the options from the command lines
# --------------------------------------------------------------------------------
# --------------------------------------------------------------------------------
while [ -n "$1" ]; do # while loop starts
    case "$1" in
    -dir) 
        EXEC_DIR="$2"
        shift
        ;;
    -pattern) 
        EXEC_PATTERN="$2"
        shift
        ;;
    *) usage "Option $1 not recognized" ; 
    esac
    shift
done
if [ -z "$EXEC_DIR" ]
then
	usage "Please specify a directory"
fi
if [ -z "$EXEC_PATTERN" ]
then
	usage "Please specify a pattern"
fi

# --------------------------------------------------------------------------------
for link in `find $EXEC_DIR -type l -name $EXEC_PATTERN`; do echo "Removing link at $link..."; cp $link notalink; unlink $link; mv notalink $link; done
# --------------------------------------------------------------------------------
# --------------------------------------------------------------------------------