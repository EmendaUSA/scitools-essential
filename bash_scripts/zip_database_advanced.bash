#!/bin/bash
# --------------------------------------------------------------------------------
# FILE NAME       : zip_database.bash
# VERSION         : 2.0 
# ORIGINAL AUTHOR : Stephane Raynaud (stephane@3dspider.com)
# -----------------------------------------------------------------------------------
# This file is provided as-is and with no support. 
# You have the right to use & to modify the file for your own and commercial use ...
# ... only if you do not remove the ORIGINAL AUTHOR Line with Stephane's information.
# -----------------------------------------------------------------------------------
VERSION=v20
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DDD_INSTALL_PATH="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
DDD_CURRENT_DIR=`pwd`
# --------------------------------------------------------------------------------
function usage() {
	echo "ERROR: $1"
	echo "Usage: zip_database.bash -db DATABASENAME.UND -zip OUTPUT.ZIP -zipuncommittedonly -zipsymlinks -debug"
	echo "DATABASENAME.UND      : The file name of the SciTools Database (must exist)"
	echo "OUTPUT.ZIP            : The file where we will zip the projects and all the files it depends on."
	echo "-zipuncommittedonly   : Optional, We will only zip the uncommitted file(s) - in general generated files."
	echo "-nozipsymlinks        : Optional, turn OFF the addition of double files if a header is a symlink. This may result in error on remote machines."
	echo "-debug                : Optional, will not delete the temporary files for debugging."
	exit
	return
}
# --------------------------------------------------------------------------------
while [ -n "$1" ]; do # while loop starts
    case "$1" in
    -db) 
        DDD_DATABASE="$2"
        shift
        ;;
    -zip) 
        DDD_ZIP="$2"
        shift
        ;;
    -debug)
        DEBUG="on"
        ;;
    -zipuncommittedonly)
        ZIPONLY="on"
        ;;
    -nozipsymlinks)
        SYMLINKS="on"
        ;;
    *) usage "Option $1 not recognized" ; 
    esac
    shift
done
# --------------------------------------------------------------------------------
if [ -z "$DDD_DATABASE" ]
then
	usage "Please specify a database"
fi
if [ -z "$DDD_ZIP" ]
then
	usage "Please specify an output zip file"
fi
# --------------------------------------------------------------------------------
echo "------------------------------------------------------------------"
echo "I will archive the database at     : $DDD_DATABASE"
echo "I will archive it at : $DDD_ZIP"
if [ -z "$DEBUG" ]
then
        echo "I will DELETE ALL the temporary files."
	echo "------------------------------------------------------------------"
fi
echo "------------------------------------------------------------------"
# ==================================================================================
# ============= [STEP 1] Delete the Existing Database
rm "$DDD_ZIP"
# ==================================================================================
# ==================================================================================
# ============= [STEP 2] Locate myself in the right directory
# Explanation: Scitools is usign the USER HOME DIR As a variable, so it will mess it
# up if imported somewhere else. Workaround is to work from ~
DDD_REAL_DATABASE=`realpath $DDD_DATABASE`
DDD_REAL_ZIP=`realpath $DDD_ZIP`
echo "I am processing everything at this location: `pwd`"
echo "------------------------------------------------------------------"
# ==================================================================================
# ============= [STEP 3.1] Creating a script to list all the files that need to 'added'
echo "Creating Python script to extract the list of project file to add."
echo "------------------------------------------------------------------"
echo 'import understand' > "$DDD_REAL_DATABASE/fileList.py"
echo 'import sys' >> "$DDD_REAL_DATABASE/fileList.py"
echo 'import os' >> "$DDD_REAL_DATABASE/fileList.py"
echo 'def fileList(db,cwd):' >> "$DDD_REAL_DATABASE/fileList.py"
echo ' for file in db.ents("File"):' >> "$DDD_REAL_DATABASE/fileList.py"
echo '  filename = file.longname()' >> "$DDD_REAL_DATABASE/fileList.py"
echo '  if ( ("jar{" in filename) or ("jmod{" in filename)):' >> "$DDD_REAL_DATABASE/fileList.py"
echo '    filename=filename[0:filename.find("{")]' >> "$DDD_REAL_DATABASE/fileList.py"
echo '  filename = filename.replace(cwd,".")' >> "$DDD_REAL_DATABASE/fileList.py"
echo '  if os.path.isfile(filename):' >> "$DDD_REAL_DATABASE/fileList.py"
echo '    print(filename)' >> "$DDD_REAL_DATABASE/fileList.py"
echo 'if __name__ == "__main__":' >> "$DDD_REAL_DATABASE/fileList.py"
echo ' args = sys.argv' >> "$DDD_REAL_DATABASE/fileList.py"
echo ' cwd = os.getcwd()' >> "$DDD_REAL_DATABASE/fileList.py"
echo ' db = understand.open(args[1])' >> "$DDD_REAL_DATABASE/fileList.py"
echo ' fileList(db,cwd)' >> "$DDD_REAL_DATABASE/fileList.py"
# ============= [STEP 3.2] Now creating the list of files by using the script
echo "The list of PROJECT files is created now using the script"
echo "------------------------------------------------------------------"
upython "$DDD_REAL_DATABASE/fileList.py" "$DDD_REAL_DATABASE" > "$DDD_REAL_DATABASE/zip.lst"
# ============= [STEP 3.2.1] We will modify the script in case we only want the non-commited files.
if [ -n "$ZIPONLY" ]
then
        # git ls-files return the local file without the "./" in front, while it's in the zip.lst
        # example: ./source/config.h vs source/config.h
        echo "The list of NEW files (not committed) is created now using git ls-files..."
        echo "------------------------------------------------------------------"
        git ls-files --others >> "$DDD_REAL_DATABASE/git.lst"
        echo "A python script is created to filter the files."
        echo "------------------------------------------------------------------"
        echo 'import sys' > "$DDD_REAL_DATABASE/updateList.py"
        echo 'import os' >> "$DDD_REAL_DATABASE/updateList.py"
        echo 'if __name__ == "__main__":' >> "$DDD_REAL_DATABASE/updateList.py"
        echo ' args=sys.argv' >> "$DDD_REAL_DATABASE/updateList.py"
        echo ' toadd={}' >> "$DDD_REAL_DATABASE/updateList.py"
        echo ' with open(args[2], "r") as gitlist:' >> "$DDD_REAL_DATABASE/updateList.py"
        echo '  lines = gitlist.readlines()' >> "$DDD_REAL_DATABASE/updateList.py"
        echo '  for line in lines:' >> "$DDD_REAL_DATABASE/updateList.py"
        echo '   toadd["./"+line] = True' >> "$DDD_REAL_DATABASE/updateList.py"
        echo ' with open(args[1], "r") as ziplist:' >> "$DDD_REAL_DATABASE/updateList.py"
        echo '  lines = ziplist.readlines()' >> "$DDD_REAL_DATABASE/updateList.py"
        echo '  for line in lines:' >> "$DDD_REAL_DATABASE/updateList.py"
        echo '   if line.startswith("./"):' >> "$DDD_REAL_DATABASE/updateList.py"
        echo '    if line in toadd:' >> "$DDD_REAL_DATABASE/updateList.py"
        echo '     print(line,end="")' >> "$DDD_REAL_DATABASE/updateList.py"
        echo '   else:' >> "$DDD_REAL_DATABASE/updateList.py"
        echo '    print(line,end="")' >> "$DDD_REAL_DATABASE/updateList.py"
        echo "We filter the files using the scripts to update the list..."
        echo "------------------------------------------------------------------"
        cp "$DDD_REAL_DATABASE/zip.lst" "$DDD_REAL_DATABASE/zip2.lst"
        upython "$DDD_REAL_DATABASE/updateList.py" "$DDD_REAL_DATABASE/zip2.lst" "$DDD_REAL_DATABASE/git.lst" > "$DDD_REAL_DATABASE/zip.lst"
        if [ -z "$DEBUG" ]
        then
                rm "$DDD_REAL_DATABASE/zip2.lst"
                rm "$DDD_REAL_DATABASE/updateList.py"
                rm "$DDD_REAL_DATABASE/git.lst"
        fi
fi
# ============= [STEP 3.2.2] Creating the SymLinks file(s) if needed:
if [ -z "$SYMLINKS" ]
then
    upython "$DDD_INSTALL_PATH/../python_scripts/symlinks_extra_files.py" -db "$DDD_REAL_DATABASE" -out "$DDD_REAL_DATABASE/symlinks.lst"
fi
# ============= [STEP 3.3] Adding all the file to the zip
echo "Adding the files to the zip"
echo "------------------------------------------------------------------"
zip "$DDD_REAL_ZIP" -@ < "$DDD_REAL_DATABASE/zip.lst"
echo "------------------------------------------------------------------"
if [ -z "$SYMLINKS" ]
then
    zip "$DDD_REAL_ZIP" -@ < "$DDD_REAL_DATABASE/symlinks.lst"
fi
# ==================== cleanup temporary files.
if [ -z "$DEBUG" ]
then
        rm "$DDD_REAL_DATABASE/fileList.py"
        rm "$DDD_REAL_DATABASE/zip.lst"
        if [ -z "$SYMLINKS" ]
        then
          rm "$DDD_REAL_DATABASE/symlinks.lst"
        fi
fi
# ==================================================================================
# ============= [STEP 5.1] Saving Current Analysis and Settings 
echo "Saving the database before modification."
echo "------------------------------------------------------------------"
rm -R /tmp/local
cp -R "$DDD_REAL_DATABASE/local" /tmp
cp "$DDD_REAL_DATABASE/settings.xml" /tmp
echo "------------------------------------------------------------------"
# ============= [STEP 5.2] Removing current Analysis
echo "Removing the local database analysis (not copied to zip)"
rm -Rf "$DDD_REAL_DATABASE/local"
echo "------------------------------------------------------------------"
# ============= [STEP 5.4] Adding database to the zip file. 
echo "Adding the und database itself to the zip"
echo "------------------------------------------------------------------"
# we have to use relative name here too...
RELATIVE_DATABASE="${DDD_REAL_DATABASE/$DDD_CURRENT_DIR/.}"
mkdir -p "$DDD_REAL_DATABASE/local"
zip -r "$DDD_REAL_ZIP" "$RELATIVE_DATABASE"
echo "------------------------------------------------------------------"
# ============= [STEP 5.5] Restoring current settings 
echo "Restoring the database after modification."
echo "------------------------------------------------------------------"
rm "$DDD_REAL_DATABASE/settings.xml"
mv /tmp/local "$DDD_REAL_DATABASE"
mv /tmp/settings.xml "$DDD_REAL_DATABASE"
# ==================================================================================
